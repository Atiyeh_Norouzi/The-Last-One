﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    [SerializeField]
    private GameObject upperPart;
    [SerializeField]
    private GameObject DownPart;

	// Use this for initialization
	void Start ()
    {
        upperPart = transform.GetChild(0).gameObject;
        DownPart = transform.GetChild(1).gameObject;
	}
    public void UpperTriggered()
    {
        //game over
    }
    public void DownTriggered()
    {
        upperPart.SetActive(false);
    }
}
