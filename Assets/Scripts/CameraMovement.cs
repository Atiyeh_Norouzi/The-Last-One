﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    private GameObject mainCamera;
    private GameObject player1;
    private GameObject player2;
    private Vector3 cameraOffset = new Vector3(3, 0, -5);
    IEnumerator MoveCamera()
    {
        while (true)
        {
            if (player1 != null && player2 != null)
                mainCamera.transform.position = (player1.transform.position + player2.transform.position)/2f + cameraOffset;
            yield return null;
        }
    }
    public void AssignPlayers(GameObject player)
    {
        if (player1 == null)
        {
            player1 = player;
        }
        else
        {
            player2 = player;
           StartCoroutine(MoveCamera());
        }
    }
}
