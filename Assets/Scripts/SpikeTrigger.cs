﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrigger : MonoBehaviour
{

    public enum spikePart { Upper , Down}
    public spikePart part;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (part == spikePart.Down)
            transform.parent.gameObject.GetComponent<Spike>().DownTriggered();
        else
            transform.parent.gameObject.GetComponent<Spike>().UpperTriggered();
    }
}
